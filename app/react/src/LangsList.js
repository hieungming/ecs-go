import { useEffect, useState } from "react";

const EmpListing = () => {
	const [empdata, empdatachange] = useState();
	const apiURL = "http://52.188.148.139:80";
	console.log(apiURL);
	useEffect(() => {
		async function logJSONData() {
			const response = await fetch(apiURL);
			const jsonData = await response.json();
			empdatachange(jsonData);
		}
		logJSONData();
	}, []);
	return (
		<div className="container">
			<div className="card">
				<div className="card-title">
					<h2>DevOps</h2>
				</div>
				<div className="card-body">
					<table className="table table-bordered">
						<thead className="bg-dark text-white">
							<tr>
								<td>Employee ID</td>
								<td>Employee Name</td>
							</tr>
						</thead>
						<tbody>
							{empdata &&
								empdata.map((item) => (
									<tr key={item.id}>
										<td>{item.id}</td>
										<td>{item.name}</td>
									</tr>
								))}
						</tbody>
					</table>
				</div>
			</div>
		</div>
	);
};

export default EmpListing;
